import 'package:flutter/material.dart';

void main() {
  runApp(ProjectAbc());
}

class ProjectAbc extends StatelessWidget {
  const ProjectAbc({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SafeArea(
        child: Scaffold(

          appBar: AppBar(
          centerTitle: true,
          title: Text('Project ABC', style: TextStyle(fontSize: 22.0,
          color: Colors.yellow,
          ),),
          leading: Icon(
            Icons.arrow_back_outlined,
            size: 24,
            color: Colors.white,
          ),
          actions: [
            Icon(Icons.mode),
            Icon(Icons.account_balance),
            Icon(Icons.comment),
          ],
        ),


          body: Center(
            child: Image.network('https://virtual.pondit.com/pluginfile.php/1/theme_edumy/headerlogo1/1669921970/pondit_logo.png',
            height: 200.0,
            width: 200.0,
            ),

            //Text('Hello Bangladesh'),
          ),
          //floatingActionButton: Icon(Icons.add),
        ),

      ),
      theme: ThemeData.light(),
    );
  }
}
